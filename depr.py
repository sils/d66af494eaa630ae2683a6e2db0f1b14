import logging
from functools import wraps


def bear_deprecated(old_name, new_name):
    """
    Emits a deprecation warning when this decorator is executed.

    >>> @module_deprecated("a", "b")
    ... def a():
    ...     return 2
    WARNING:root:'a' is deprecated. Use 'b' instead!

    >>> a()
    2

    :param old_name:
    :param new_name:
    :return:
    """
    logging.warning("'{}' is deprecated. Use '{}' instead!".format(old_name,
                                                                   new_name))
    return wraps
